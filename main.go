/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"errors"
	"flag"
	"os"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.
	_ "k8s.io/client-go/plugin/pkg/client/auth"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	lemonldapngorgv1beta1 "lemonldap-ng.org/llng-operator/v2/api/v1beta1"
	"lemonldap-ng.org/llng-operator/v2/controllers"
	// +kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(lemonldapngorgv1beta1.AddToScheme(scheme))
	// +kubebuilder:scaffold:scheme
}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	var probeAddr string

	var mode string

	var baseCmName string
	var baseCmNamespace string

	var targetCmName string
	var targetCmNamespace string

	var managerBaseUrl string

	flag.StringVar(&metricsAddr, "main-bind-address", ":8080", "The address the config is served")
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")

	flag.StringVar(&mode, "mode", "", "The mode to be used, can be : ConfigMapToConfigMap, ConfigMapToManagerAPI, ManagerToManager")

	flag.StringVar(&baseCmName, "base-cm-name", "", "The name of the cm used as base config (config should be under config.json key)")
	flag.StringVar(&baseCmNamespace, "base-cm-namespace", "", "The namespace of the cm used as base config (config should be under config.json key)")

	flag.StringVar(&targetCmName, "target-cm-name", "", "The namespace of the cm where the config should be stored (config.json key will be populated)")
	flag.StringVar(&targetCmNamespace, "target-cm-namespace", "", "The namespace of the cm where the config should be stored (config.json key will be populated)")

	flag.StringVar(&managerBaseUrl, "manager-base-url", "", "The base url of the llng manager (used to push/retrieve configs) The controller must be allowed to access it")

	opts := zap.Options{
		Development: true,
	}
	opts.BindFlags(flag.CommandLine)

	setupLog.Info("starting.... parsing args")

	flag.Parse()
	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	setupLog.Info("starting...")

	switch mode {
	case "ConfigMapToConfigMap":
		verifyConfigMapToConfigMapConfig(baseCmName, baseCmNamespace, targetCmName, targetCmNamespace)
		break
	case "ConfigMapToManagerApi":
		verifyConfigMapToManagerApiConfig(baseCmName, baseCmNamespace, managerBaseUrl)
		break
	case "ManagerApiToManagerApi":
		verifyManagerApiToManagerApiConfig(managerBaseUrl)
		break
	default:
		setupLog.Error(errors.New("args: invalid mode"), "argument error", "arg", "mode", "arg_value", mode)
		os.Exit(1)
	}

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "fcde3473.lemonldap-ng.org",
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	// start the VHost reconciler
	if err = (&controllers.VHostReconciler{
		BaseReconciler: controllers.BaseReconciler{
			Client:             mgr.GetClient(),
			Log:                ctrl.Log.WithName("controllers").WithName("VHost"),
			Scheme:             mgr.GetScheme(),
			Mode:               mode,
			BaseCmName:         baseCmName,
			BaseCmNamespace:    baseCmNamespace,
			TargetCmName:       targetCmName,
			TargetCmNamespace:  targetCmNamespace,
			LLNGManagerBaseUrl: managerBaseUrl,
		},
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "VHost")
		os.Exit(1)
	}

	// +kubebuilder:scaffold:builder

	// start the ConfigMap reconciler
	// TODO doesnt start in ManagerToManager mode
	if err = (&controllers.ConfigMapReconciler{
		BaseReconciler: controllers.BaseReconciler{
			Client:             mgr.GetClient(),
			Log:                ctrl.Log.WithName("controllers").WithName("ConfigMap"),
			Scheme:             mgr.GetScheme(),
			Mode:               mode,
			BaseCmName:         baseCmName,
			BaseCmNamespace:    baseCmNamespace,
			TargetCmName:       targetCmName,
			TargetCmNamespace:  targetCmNamespace,
			LLNGManagerBaseUrl: managerBaseUrl,
		},
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "VHost")
		os.Exit(1)
	}
	// +kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("health", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("check", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}

// TODO may improve the validation
func verifyConfigMapToConfigMapConfig(baseCmName string, baseCmNamespace string, targetCmName string, targetCmNamespace string) {
	if baseCmName == "" {
		setupLog.Error(errors.New("args: missing base-cm-name"), "argument error", "arg", "base-cm-name", "arg_value", baseCmName)

		os.Exit(1)
	} else if baseCmNamespace == "" {
		setupLog.Error(errors.New("args: missing base-cm-namespace"), "argument error", "arg", "base-cm-namespace", "arg_value", baseCmNamespace)

		os.Exit(1)
	} else if targetCmName == "" {
		setupLog.Error(errors.New("args: missing target-cm-name"), "argument error", "arg", "target-cm-name", "arg_value", targetCmName)

		os.Exit(1)
	} else if targetCmNamespace == "" {
		setupLog.Error(errors.New("args: missing target-cm-namespace"), "argument error", "arg", "target-cm-namespace", "arg_value", targetCmNamespace)

		os.Exit(1)
	}
	return
}

// TODO may improve the validation
func verifyConfigMapToManagerApiConfig(baseCmName string, baseCmNamespace string, managerBaseUrl string) {
	if baseCmName == "" {
		setupLog.Error(errors.New("args: missing base-cm-name"), "argument error", "arg", "base-cm-names", "arg_value", baseCmName)

		os.Exit(1)
	} else if baseCmNamespace == "" {
		setupLog.Error(errors.New("args: missing base-cm-namespace"), "argument error", "arg", "base-cm-namespace", "arg_value", baseCmNamespace)

		os.Exit(1)
	} else if managerBaseUrl == "" {
		setupLog.Error(errors.New("args: missing manager-base-url"), "argument error", "arg", "manager-base-url", "arg_value", managerBaseUrl)

		os.Exit(1)
	}
	return
}

// TODO may improve the validation
func verifyManagerApiToManagerApiConfig(managerBaseUrl string) {
	if managerBaseUrl == "" {
		setupLog.Error(errors.New("args: missing manager-base-url"), "argument error", "arg", "manager-base-url", "arg_value", managerBaseUrl)

		os.Exit(1)
	}
	return
}
