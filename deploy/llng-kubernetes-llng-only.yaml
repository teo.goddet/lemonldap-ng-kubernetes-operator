apiVersion: v1
kind: Namespace
metadata:
  labels:
    app: llng
  name: auth
spec:
  finalizers:
  - kubernetes
status:
  phase: Active
---
apiVersion: v1
data:
  lemonldap-ng.ini: ";==============================================================================\n; LemonLDAP::NG local configuration parameters\n;\n; This file is dedicated to configuration parameters override\n; You can set here configuration parameters that will be used only by\n; local LemonLDAP::NG elements\n;\n; Section \"all\" is always read first before \"portal\", \"handler\"\n; and \"manager\"\n;\n; Section \"configuration\" is used to load global configuration and set cache\n; (replace old storage.conf file)\n;\n; Other section are only read by the specific LemonLDAP::NG component\n;==============================================================================\n[all]\n; CUSTOM FUNCTION\n; If you want to create customFunctions in rules, declare them here:\n;require = Package\n; Prevent Portal to crash if Perl module is not found\n;requireDontDie = 1\n;customFunctions = function1 function2\n;customFunctions = Package::func1 Package::func2\n; CROSS-DOMAIN\n; If you have some handlers that are not registered on the main domain,\n; uncomment this\ncda = 1\n; SAFE JAIL\n; Uncomment this to disable Safe jail.\n; Warning: this can allow malicious code in custom functions or rules\n;useSafeJail = 0\n; LOGGING\n;\n; 1 - Defined logging level\n;   Set here one of error, warn, notice, info or debug\nlogLevel     = warn\n; Note that this has no effect for Apache2 logging: Apache LogLevel is used\n; instead\n;\n; 2 - Change logger\n;\n;   By default, logging is set to:\n;    - Lemonldap::NG::Common::Logger::Apache2  for ApacheMP2 handlers\n;    - Lemonldap::NG::Common::Logger::Syslog   for FastCGI (Nginx)\n;    - Lemonldap::NG::Common::Logger::Std      for PSGI applications (manager,\n;                                              portal,...) when they are not\n;                                              launched by FastCGI server\n;   Other loggers availables:\n;    - Lemonldap::NG::Common::Logger::Log4perl to use Log4perl\n;\n;   \"Std\" is redirected to the web server logs for Apache. For Nginx, only if\n;   request failed\n;\n;   You can overload this in this section (for all) or in another section if\n;   you want to change logger for a specified app.\n;\n;   LLNG uses 2 loggers: 1 for technical logs (logger), 1 for user actions\n;   (userLogger). \"userLogger\" uses the same class as \"logger\" if not set.\nlogger     = Lemonldap::NG::Common::Logger::Std\nuserLogger = Lemonldap::NG::Common::Logger::Std\n;\n; 2.1 - Using Syslog\n;\n;   For Syslog logging, you can also overwrite facilities. Default values:\n;logger             = Lemonldap::NG::Common::Logger::Syslog\n;syslogFacility     = daemon\n;syslogOptions      = cons,pid,ndelay\n;userSyslogFacility = auth\n;userSyslogOptions  = cons,pid,ndelay\n;\n; 2.2 - Using Log4perl\n;\n;   If you want to use Log4perl, you can set these parameters. Here are default\n;   values:\n;logger             = Lemonldap::NG::Common::Logger::Log4perl\n;log4perlConfFile   = /etc/log4perl.conf\n;log4perlLogger     = LLNG\n;log4perlUserLogger = LLNG.user\n;\n;   Here, Log4perl configuration is read from /etc/log4perl.conf. The \"LLNG\"\n;   value points to the logger class. Example:\n;     log4perl.logger.LLNG      = WARN, File1\n;     log4perl.logger.LLNG.user = INFO, File2\n;     ...\n; CONFIGURATION CHECK\n;\n; LLNG verify configuration at server start. If you use \"reload\" mechanism,\n; local cache will be updated. Configuration is checked locally every\n; 10 minutes by each LLNG component. You can change this value using\n; `checkTime` (time in seconds).\n; To increase performances, you should comment this parameter and rely on cache.\ncheckTime = 1\n[configuration]\n; confTimeout: maximum time to get configuration (default 10)\n;confTimeout = 5\n;type         = REST\n;baseUrl      = http://api.auth.svc/config\n;proxyOptions = { timeout => 5 }\ntype=File\ndirName = /var/lib/lemonldap-ng/conf\nprettyPrint = 1\n; LOCAL CACHE CONFIGURATION\n;\n; To increase performances, use a local cache for the configuration. You have\n; to choose a Cache::Cache module and set its parameters. Example:\n;\n;           localStorage = Cache::FileCache\n;           localStorageOptions={                             \\\n;               'namespace'          => 'lemonldap-ng-config',\\\n;               'default_expires_in' => 600,                  \\\n;               'directory_umask'    => '007',                \\\n;               'cache_root'         => '/var/cache/lemonldap-ng',       \\\n;               'cache_depth'        => 3,                    \\\n;           }\n;localStorage=Cache::FileCache\n;localStorageOptions={                             \\\n;    'namespace'          => 'lemonldap-ng-config',\\\n;    'default_expires_in' => 600,                  \\\n;    'directory_umask'    => '007',                \\\n;    'cache_root'         => '/var/cache/lemonldap-ng',       \\\n;    'cache_depth'        => 3,                    \\\n;}\n; Session Storage \n;globalStorage = Lemonldap::NG::Common::Apache::Session::REST\n;globalStorageOptions = { 'baseUrl' => 'http://api.auth.svc/sessions'}\n# the manager is used as the real on disk session storage (others use rest)\n;globalStorage = Apache::Session::File\n;globalStorageOptions = { 'Directory' => '/var/lib/lemonldap-ng/sessions/', 'LockDirectory' => '/var/lib/lemonldap-ng/sessions/lock/',  'generateModule' => 'Lemonldap::NG::Common::Apache::Session::Generate::SHA256'}\n[portal]\nrestSessionServer=1\nrestConfigServer=1\n; PORTAL CUSTOMIZATION\n; I - Required parameters\n; staticPrefix: relative (or URL) location of static HTML components\nstaticPrefix = /static\n; location of HTML templates directory\ntemplateDir  = /usr/share/lemonldap-ng/portal/templates\n; languages: available languages for portal interface\nlanguages    = en, fr, vi, it, ar, de, fi, tr, pl, zh_TW, es\n[handler]\n[manager]\n; Manager protection: by default, the manager is protected by a demo account.\n; You can protect it :\n; * by Apache itself,\n; * by the parameter 'protection' which can take one of the following\n; values :\n;   * authenticate : all authenticated users can access\n;   * manager      : manager is protected like other virtual hosts: you\n;                    have to set rules in the corresponding virtual host\n;   * <rule>       : you can set here directly the rule to apply\n;   * none         : no protection\nprotection   = none\n; staticPrefix: relative (or URL) location of static HTML components\nstaticPrefix = /static\n;\n; instanceName: Display current LLNG instance into Manager\n;instanceName = Demo\n; location of HTML templates directory\ntemplateDir  = /usr/share/lemonldap-ng/manager/htdocs/templates\n; languages: available languages for manager interface\nlanguages    = en, fr, it, vi, ar, tr, pl, zh_TW, es\n; Manager modules enabled\n; Set here the list of modules you want to see in manager interface\n; The first will be used as default module displayed\nenabledModules = conf, viewer, sessions, notifications, 2ndFA\n;enabledModules = conf, sessions, notifications, 2ndFA\n; To avoid restricted users to edit configuration, defaulModule MUST be different than 'conf'\n; 'conf' is set by default\ndefaultModule = viewer\n; Viewer module allows us to edit configuration in read-only mode\n; Options can be set with specific rules like this :\n;viewerAllowBrowser = $uid eq 'dwho'\n;viewerAllowDiff = $uid ne 'dwho'\n;\n; Viewer options - Default values\n;viewerHiddenKeys = samlIDPMetaDataNodes samlSPMetaDataNodes managerPassword ManagerDn globalStorageOptions persistentStorageOptions\nviewerAllowBrowser = 1\nviewerAllowDiff = 1\n"
kind: ConfigMap
metadata:
  labels:
    app: llng
  name: llng-ini-config
  namespace: auth
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: llng
    component: api
  name: llng-api
  namespace: auth
spec:
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 9999
  selector:
    app: llng
    component: main
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: llng
    component: generic-handler
  name: llng-generic-handler
  namespace: auth
spec:
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 8080
  selector:
    app: llng
    component: main
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: llng
    component: manager
  name: llng-manager
  namespace: auth
spec:
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 8888
  selector:
    app: llng
    component: main
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: llng
    component: portal
  name: llng-portal
  namespace: auth
spec:
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: llng
    component: main
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  labels:
    app: llng
  name: llng-config
  namespace: auth
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 2G
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: llng
  name: llng-main
  namespace: auth
spec:
  replicas: 1
  selector:
    matchLabels:
      app: llng
      component: main
  template:
    metadata:
      labels:
        app: llng
        component: main
    spec:
      containers:
      - image: llng-kubernetes:1.0.0
        name: llng-portal
        ports:
        - containerPort: 80
          name: portal
        - containerPort: 8080
          name: handler
        - containerPort: 8888
          name: manager
        - containerPort: 9999
          name: api
        volumeMounts:
        - mountPath: /var/lib/lemonldap-ng/conf
          name: config
        - mountPath: /etc/lemonldap-ng
          name: ini-config
      volumes:
      - name: config
        persistentVolumeClaim:
          claimName: llng-config
      - configMap:
          name: llng-ini-config
        name: ini-config
---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  labels:
    app: llng
    component: manager
  name: llng-manager
  namespace: auth
spec:
  entryPoints:
  - admin
  routes:
  - kind: Rule
    match: Host(`manager.example.com`)
    services:
    - name: manager
      port: 80
---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  labels:
    app: llng
    component: portal
  name: llng-portal
  namespace: auth
spec:
  entryPoints:
  - web
  routes:
  - kind: Rule
    match: Host(`auth.example.com`)
    services:
    - name: portal
      port: 80
---
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  labels:
    app: llng
  name: llng-sso-auth
  namespace: auth
spec:
  forwardAuth:
    address: http://generic-handler.auth.svc.cluster.local/auth
    authResponseHeadersRegex: ^.*$
