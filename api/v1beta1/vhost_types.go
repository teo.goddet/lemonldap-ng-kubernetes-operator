/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// +k8s:openapi-gen=true
// VHostSpec defines the desired state of VHost
type VHostSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	Host            string            `json:"host"`
	LocationRules   []LocationRule    `json:"locationRules"`
	ExportedHeaders []ExportedHeaders `json:"exportedHeaders,omitempty"`
	Options         Options           `json:"options,omitempty"`
}

// See https://lemonldap-ng.org/documentation/latest/writingrulesand_headers.html
type LocationRule struct {
	Location string `json:"location"`
	Rule     string `json:"rule"`
}

// See https://lemonldap-ng.org/documentation/latest/writingrulesand_headers.html
type ExportedHeaders struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

// Aliases is not available here to avoid conflicts
// TODO maybe add aliases
type Options struct {
	// Used to build redirection URL (when user is not logged, or for CDA requests) (-1 default)
	// +kubebuilder:default:=-1

	Port int `json:"port,omitempty"`
	// Used to build redirection URL (when user is not logged, or for CDA requests) (-1 default, 0 http, 1 https)
	// +kubebuilder:default:=-1

	HTTPS int `json:"https,omitempty"`

	// Maintenance mode (block all reuquest with a maintenance message (0 off, 1 on)
	// +kubebuilder:default:=false
	Maintenance bool `json:"maintenance,omitempty"`

	// TODO document what is it !
	// +kubebuilder:doc:note=Dont Really KNOW :/ Doc is : can be used for overwriting REMOTE_CUSTOM with a custom function. Provide a comma separated parameters list with custom function path and args. By example: My::accessToTrace, Doctor, Who
	AccessToTrace string `json:"accessToTrace,omitempty"`

	// Handler type (normal, ServiceToken Handler, DevOps Handler,...) See https://lemonldap-ng.org/documentation/latest/index_handler.html
	// +kubebuilder:default:=Main
	Type string `json:"type,omitempty"`

	// This option avoids to reject user with a rule based on $_authenticationLevel. When user has not got the required level, he is redirected to an upgrade page in the portal. This default level is required for ALL locations relative to this virtual host. It can be overrided for each locations.
	RequiredAuthenticationLevel int `json:"requiredAuthenticationLevel,omitempty"`

	// By default, ServiceToken is just valid during 30 seconds. This TTL can be customized for each virtual host
	ServiceTokenTimeout int `json:"serviceTokenTimeout,omitempty"`
}

// VHostStatus defines the observed state of VHost
type VHostStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// VHost is the Schema for the vhosts API
type VHost struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   VHostSpec   `json:"spec,omitempty"`
	Status VHostStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// VHostList contains a list of VHost
type VHostList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []VHost `json:"items"`
}

func init() {
	SchemeBuilder.Register(&VHost{}, &VHostList{})
}
