## A Kubernetes Operator for LemonLDAP NG
This repo aims to provide to things : 
### A K8S tuned lemonldap image
The kubernetes-lemonldap-ng image provide 4 services (accesible by any host) : 
 - portal 
 - api (portal rest services)
 - manager
 - generic-handler

 It's ini file is loaded from a configMap. 
 It provide a generic handler that can be used as Traefik ForwardAuth middleware target. (Should also work with nginx authrequest).

### A K8S config provider
It's a controller and some crd able to update lemon ldap configuration based on kubernetes objects such as VHost or Application.

## Get Started
```kubectl apply -f ./deploy```

## State of the project
This project is looking for maintainers. 
It must be considered as beta. 

## Planned Functionalities
- [x] Provide a Base config by ConfigMap or with the manager
- [x] Update a ConfigMap or the manager config using a http call
- [x] VHost CRD (locationsRules, exportedHeaders, post)
- [] App CRD (Maybe Also Categories)
- [] SAML/Oauth/CAS service CRD

## Not Planned
 -  Handle Name Conflicts in Application or VHost

## TODO : 
- [] have transpiler tests working
- [] find a way to handle applications and categories (need to handle unique order number)
An idea would be that user declare their app (App Crd) (maybe they request an order and category)
And that a admin Classify/order them using another crd (refercing the app crd)
- [] Verify what happen when reaching high config numbers.
- [] Add a watch to the Deployment/Pod to populate their config when they start.


## Some ideas
### config crd
Maybe we should have another crd instead of the configmap for the base config.
We could take advantage of k8s validation and kubebuilder defaults
It's a high maintenance costs (follow each llng release to follow the conf)

### rest endpoint
We could serve the config as a rest server backend.

## Test It
Setup a kind Cluster,
Apply the /deploy to it

Use port forward to access portal and manager

## How to release
Do your changes :)
```make generate```
Set IMG et LLNG_IMG env var accordingly (teogoddet/<image name>/<tag>)

```make docker-build```
```make docker-push```
```make generate-deploy``` (don't forget to commit the generated deploy directory)

Create a new git tag

Create a new release with a correct changelog
