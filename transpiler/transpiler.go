/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package transpiler

import (
	"encoding/json"
	"fmt"
	lemonldapngorgv1beta1 "lemonldap-ng.org/llng-operator/v2/api/v1beta1"
	"time"
)

// types used to parse/generate lemonldap config
type llConfig struct {
	ExportedHeaders map[string]llExportedHeader `json:"exportedHeaders"`
	LocationRules   map[string]llLocationRule   `json:"locationRules"`
	VhostOptions    llOptions                   `json:"vhostOptions"`
	Post            map[string]llPost           `json:"post"`
}

type llExportedHeader = map[string]string

type llLocationRule = map[string]string

type llOptions struct {
	Port                        int    `json:"vhostPort,omitempty"`
	HTTPS                       int    `json:"vhostHttps,omitempty"`
	Maintenance                 bool   `json:"vhostMaintenance,omitempty"`
	AccessToTrace               string `json:"vhostAccessToTrace,omitempty"`
	RequiredAuthenticationLevel int    `json:"vhostAuthnLevel,omitempty"`
	ServiceTokenTimeout         int    `json:"vhostServiceTokenTTL,omitempty"`
}

type llPost struct {
}

func GetUpdatedConfig(actualConfig []byte, vhostlist []lemonldapngorgv1beta1.VHost) ([]byte, error) {
	var err error = nil

	conf := make(map[string]*json.RawMessage)
	if err = json.Unmarshal(actualConfig, &conf); err != nil {
		fmt.Printf("goooooooooooooooal %#v\n", err)
		return nil, err
	}

	locationRules := make(map[string]llLocationRule)

	if conf["locationRules"] != nil {
		if err = json.Unmarshal(*conf["locationRules"], &locationRules); err != nil {
			return nil, err
		}
	}

	for _, vhost := range vhostlist {
		locationRules[vhost.Spec.Host] = make(map[string]string)
		for _, locationRule := range vhost.Spec.LocationRules {
			locationRules[vhost.Spec.Host][locationRule.Location] = locationRule.Rule
		}
	}
	var newLocationRules json.RawMessage
	newLocationRules, err = json.Marshal(locationRules)
	if err != nil {
		return nil, err
	}

	conf["locationRules"] = &newLocationRules

	exportedHeaders := make(map[string]llExportedHeader)

	if conf["exportedHeaders"] != nil {
		if err := json.Unmarshal(*conf["exportedHeaders"], &exportedHeaders); err != nil {
			return nil, err
		}
	}

	for _, vhost := range vhostlist {
		exportedHeaders[vhost.Spec.Host] = make(map[string]string)
		for _, exportedHeader := range vhost.Spec.ExportedHeaders {
			exportedHeaders[vhost.Spec.Host][exportedHeader.Name] = exportedHeader.Value
		}
	}
	var newExportedHeaders json.RawMessage
	newExportedHeaders, err = json.Marshal(exportedHeaders)
	if err != nil {
		return nil, err
	}

	conf["exportedHeaders"] = &newExportedHeaders

	vhostOptions := make(map[string]llOptions)

	if conf["vhostOptions"] != nil {
		if err := json.Unmarshal(*conf["vhostOptions"], &vhostOptions); err != nil {
			return nil, err
		}
	}

	for _, vhost := range vhostlist {
		vhostOptions[vhost.Spec.Host] = llOptions{
			Port:                        vhost.Spec.Options.Port,
			HTTPS:                       vhost.Spec.Options.HTTPS,
			Maintenance:                 vhost.Spec.Options.Maintenance,
			AccessToTrace:               vhost.Spec.Options.AccessToTrace,
			RequiredAuthenticationLevel: vhost.Spec.Options.RequiredAuthenticationLevel,
			ServiceTokenTimeout:         vhost.Spec.Options.ServiceTokenTimeout,
		}
	}
	var newVHostOptions json.RawMessage
	newVHostOptions, err = json.Marshal(vhostOptions)
	if err != nil {
		return nil, err
	}

	conf["vhostOptions"] = &newVHostOptions

	var author json.RawMessage
	author, _ = json.Marshal("LemonLDAP NG Kubernetes Operator")
	var date json.RawMessage
	date, _ = json.Marshal(time.Now().Unix())

	conf["cfgAuthor"] = &author
	conf["cfgDate"] = &date

	config, err := json.Marshal(conf)
	if err != nil {
		return nil, err
	}
	return config, nil
}
