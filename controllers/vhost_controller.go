/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	ctrl "sigs.k8s.io/controller-runtime"

	lemonldapngorgv1beta1 "lemonldap-ng.org/llng-operator/v2/api/v1beta1"
)

// VHostReconciler reconciles a VHost object
type VHostReconciler struct {
	BaseReconciler
}

// +kubebuilder:rbac:groups=lemonldap-ng.org,resources=vhosts,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the VHost object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.7.0/pkg/reconcile
func (r *VHostReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	switch r.Mode {
	case "ConfigMapToConfigMap":
		return r.ConfigMapToConfigMap(ctx, req)
	case "ConfigMapToManagerApi":
		return r.ConfigMapToManagerApi(ctx, req)
	case "ManagerApiToManagerApi":
		return r.ManagerApiToManagerApi(ctx, req)
	}
	return ctrl.Result{}, nil // never happens
}

// SetupWithManager sets up the controller with the Manager.
func (r *VHostReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&lemonldapngorgv1beta1.VHost{}).
		Complete(r)
}
