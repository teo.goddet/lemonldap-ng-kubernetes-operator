/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	lemonldapngorgv1beta1 "lemonldap-ng.org/llng-operator/v2/api/v1beta1"

	"lemonldap-ng.org/llng-operator/v2/transpiler"
)

// +kubebuilder:rbac:groups=lemonldap-ng.org,resources=vhosts,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=configmaps,verbs=get;list;watch;create;update;patch;delete

func (r *BaseReconciler) ConfigMapToConfigMap(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("object", req.NamespacedName)

	// retrieve vhost
	var vhostlist lemonldapngorgv1beta1.VHostList
	if err := r.Client.List(ctx, &vhostlist); err != nil {
		log.Error(err, "unable to fetch VHosts")
		// we'll ignore not-found errors, since they can't be fixed by an immediate
		// requeue (we'll need to wait for a new notification), and we can get them
		// on deleted requests.
		return ctrl.Result{}, err
	}

	// retrieve base configMap
	var basecm corev1.ConfigMap
	if err := r.Client.Get(ctx, client.ObjectKey{Namespace: r.BaseCmNamespace, Name: r.BaseCmName}, &basecm); err != nil {
		log.Error(err, "unable to fetch base config map, you need to add your llng config as a configmap and set up the required flags")
		return ctrl.Result{}, err
	}

	updatedConf, err := transpiler.GetUpdatedConfig([]byte(basecm.Data["lmConf-1.json"]), vhostlist.Items)
	if err != nil {
		log.Error(err, "unable to transpile the config")
		return ctrl.Result{}, err
	}

	// build the target cm
	targetCm := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Labels:      make(map[string]string),
			Annotations: make(map[string]string),
			Name:        r.TargetCmName,
			Namespace:   r.TargetCmNamespace,
		},
		Data: make(map[string]string),
	}

	op, err := ctrl.CreateOrUpdate(context.TODO(), r.Client, targetCm, func() error {
		targetCm.Data["lmConf-1.json"] = string(updatedConf)
		return nil
	})

	if err != nil {
		log.Error(err, "unable to create/update config map", "cm", targetCm)
		return ctrl.Result{}, err
	}

	log.V(1).Info("created config map", "cm", targetCm, "op", op)

	// handle op (restart if needed)

	return ctrl.Result{}, nil
}
