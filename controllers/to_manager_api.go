/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"

	ctrl "sigs.k8s.io/controller-runtime"

	"sigs.k8s.io/controller-runtime/pkg/client"

	corev1 "k8s.io/api/core/v1"
	lemonldapngorgv1beta1 "lemonldap-ng.org/llng-operator/v2/api/v1beta1"

	"lemonldap-ng.org/llng-operator/v2/transpiler"
)

// +kubebuilder:rbac:groups=lemonldap-ng.org,resources=vhosts,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=configmaps,verbs=get;list;watch;create;update;patch;delete

func (r *BaseReconciler) ConfigMapToManagerApi(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("module", "ConfigMapToManagerApi")

	// retrieve vhost
	var vhostlist lemonldapngorgv1beta1.VHostList
	if err := r.Client.List(ctx, &vhostlist); err != nil {
		log.Error(err, "unable to fetch VHosts")
		// we'll ignore not-found errors, since they can't be fixed by an immediate
		// requeue (we'll need to wait for a new notification), and we can get them
		// on deleted requests.
		return ctrl.Result{}, err
	}

	// retrieve base configMap
	var basecm corev1.ConfigMap
	if err := r.Client.Get(ctx, client.ObjectKey{Namespace: r.BaseCmNamespace, Name: r.BaseCmName}, &basecm); err != nil {
		log.Error(err, "unable to fetch base config map")
		return ctrl.Result{}, err
	}

	updatedConf, err := transpiler.GetUpdatedConfig([]byte(basecm.Data["lmConf-1.json"]), vhostlist.Items)
	if err != nil {
		log.Error(err, "unable to transpile the config, check your base config json syntax")
		return ctrl.Result{}, err
	}

	newConfigId, err := push(r.LLNGManagerBaseUrl, updatedConf)
	if err != nil {
		log.Error(err, "unable to push config", "url", r.LLNGManagerBaseUrl)
		return ctrl.Result{}, err
	}

	log.V(1).Info("updated config", "newConfigId", newConfigId)

	return ctrl.Result{}, nil
}

func (r *BaseReconciler) ManagerApiToManagerApi(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("module", "ManagerApiToManagerApi")

	// retrieve vhost
	var vhostlist lemonldapngorgv1beta1.VHostList
	if err := r.Client.List(ctx, &vhostlist); err != nil {
		log.Error(err, "unable to fetch VHosts")
		// we'll ignore not-found errors, since they can't be fixed by an immediate
		// requeue (we'll need to wait for a new notification), and we can get them
		// on deleted requests.
		return ctrl.Result{}, err
	}

	actualConf, err := get(r.LLNGManagerBaseUrl)
	if err != nil {
		log.Error(err, "unable to get the config fron the manager")
		return ctrl.Result{}, err
	}
	// TODO need to handle deleted vhosts (maybe this controller should handle only the modified vhost)

	updatedConf, err := transpiler.GetUpdatedConfig([]byte(actualConf), vhostlist.Items)
	if err != nil {
		log.Error(err, "unable to transpile the config")
		return ctrl.Result{}, err
	}
	newConfigId, err := push(r.LLNGManagerBaseUrl, updatedConf)

	if err != nil {
		log.Error(err, "unable to push config", "url", r.LLNGManagerBaseUrl)
		return ctrl.Result{}, err
	}

	log.V(1).Info("updated config", "newConfigId", newConfigId)

	return ctrl.Result{}, nil // TODO check for periodic reschuedule ?
}

func push(baseUrl string, payload []byte) (string, error) {

	httpClient := http.Client{
		Timeout: time.Second * 10,
	}

	req, err := http.NewRequest(http.MethodPost, baseUrl+"/confs/raw", bytes.NewBuffer(payload))
	if err != nil {
		return "", err
	}

	req.Header.Set("User-Agent", "llng-config-controller")
	req.Header.Set("Content-Type", "application/json")

	res, err := httpClient.Do(req)
	if err != nil {
		return "", err
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	result := make(map[string]*json.RawMessage)
	err = json.Unmarshal(body, &result)
	if err != nil {
		return "", err
	}

	if string(*result["result"]) != "1" {
		return "", errors.New(string(*result["message"]))
	}

	return string(*result["cfgNum"]), nil
}

func get(baseUrl string) ([]byte, error) {

	httpClient := http.Client{
		Timeout: time.Second * 10,
	}

	req, err := http.NewRequest(http.MethodGet, baseUrl+"/confs/latest?full=1", nil) // TODO use path merge
	if err != nil {
		return []byte(``), err
	}

	req.Header.Set("User-Agent", "llng-config-controller")
	req.Header.Set("Accept", "application/json")

	res, err := httpClient.Do(req)
	if err != nil {
		return []byte(``), err
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return []byte(``), err
	}

	return body, nil
}
