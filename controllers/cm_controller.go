/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	corev1 "k8s.io/api/core/v1"
)

// ConfigMapReconciler reconciles a ConfigMap object
type ConfigMapReconciler struct {
	BaseReconciler
}

// +kubebuilder:rbac:groups="",resources=configmaps,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=lemonldap-ng.org,resources=vhosts,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the ConfigMap object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.7.0/pkg/reconcile
func (r *ConfigMapReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	switch r.Mode {
	case "ConfigMapToConfigMap":
		return r.ConfigMapToConfigMap(ctx, req)
	case "ConfigMapToManagerApi":
		return r.ConfigMapToManagerApi(ctx, req)
	}
	return ctrl.Result{}, nil // never happens
}

func selectConfigMapPredicate(r *ConfigMapReconciler) predicate.Predicate {
	return predicate.Funcs{
		GenericFunc: func(e event.GenericEvent) bool {
			switch r.Mode {
			case "ConfigMapToConfigMap":
				return (e.Object.GetName() == r.BaseCmName && e.Object.GetNamespace() == r.BaseCmNamespace) || (e.Object.GetName() == r.TargetCmName && e.Object.GetNamespace() == r.TargetCmNamespace)
			case "ConfigMapToManagerApi":
				return (e.Object.GetName() == r.BaseCmName && e.Object.GetNamespace() == r.BaseCmNamespace)
			case "ManagerApiToManagerApi":
				return false
			}
			return false
		},
		CreateFunc: func(e event.CreateEvent) bool {
			switch r.Mode {
			case "ConfigMapToConfigMap":
				return (e.Object.GetName() == r.BaseCmName && e.Object.GetNamespace() == r.BaseCmNamespace) || (e.Object.GetName() == r.TargetCmName && e.Object.GetNamespace() == r.TargetCmNamespace)
			case "ConfigMapToManagerApi":
				return (e.Object.GetName() == r.BaseCmName && e.Object.GetNamespace() == r.BaseCmNamespace)
			case "ManagerApiToManagerApi":
				return false
			}
			return false
		},
		UpdateFunc: func(e event.UpdateEvent) bool {
			switch r.Mode {
			case "ConfigMapToConfigMap":
				return (e.ObjectNew.GetName() == r.BaseCmName && e.ObjectNew.GetNamespace() == r.BaseCmNamespace) || (e.ObjectNew.GetName() == r.TargetCmName && e.ObjectNew.GetNamespace() == r.TargetCmNamespace)
			case "ConfigMapToManagerApi":
				return (e.ObjectNew.GetName() == r.BaseCmName && e.ObjectNew.GetNamespace() == r.BaseCmNamespace)
			case "ManagerApiToManagerApi":
				return false
			}
			return false
		},
		DeleteFunc: func(e event.DeleteEvent) bool {
			switch r.Mode {
			case "ConfigMapToConfigMap":
				return (e.Object.GetName() == r.BaseCmName && e.Object.GetNamespace() == r.BaseCmNamespace) || (e.Object.GetName() == r.TargetCmName && e.Object.GetNamespace() == r.TargetCmNamespace)
			case "ConfigMapToManagerApi":
				return (e.Object.GetName() == r.BaseCmName && e.Object.GetNamespace() == r.BaseCmNamespace)
			case "ManagerApiToManagerApi":
				return false
			}
			return false
		},
	}
}

// SetupWithManager sets up the controller with the Manager.
func (r *ConfigMapReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&corev1.ConfigMap{}).
		WithEventFilter(selectConfigMapPredicate(r)).
		Complete(r)
}
